# pink-session

EPICS IOC with session records

## docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-bl/pink-session/session:v1.0
    container_name: session
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pink-session/iocBoot/iocsts"
    command: "./session.cmd"
    environment:
      - IOCBL=PINK
      - IOCDEV=SESSION
```
