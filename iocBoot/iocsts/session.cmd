#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## docker macros
#epicsEnvSet("IOCBL", "PINK")
#epicsEnvSet("IOCDEV", "SESSION")

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("session.db","BL=$(IOCBL),DEV=$(IOCDEV)")

iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
